## INSTALL

- check for PHP version ^8.0.2 
- git clone https://gitlab.com/alenp/laraveltest.git
- cd laraveltest
- c/p provided .env file to the root directory
- composer update
- composer install
- php artisan serve
- Visit http://localhost:8000 or https://j.olyntha.com

## Runing CLI commands
- php artisan create:author (Enter your API email / password for authorization)
