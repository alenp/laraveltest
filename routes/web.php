<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\BooksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login');
Route::get('/login', [AuthController::class, 'loginForm']);
Route::post('/login', [AuthController::class, 'login'])->name('login.post');
Route::get('/logout', [AuthController::class, 'logout']);

Route::middleware(['logged.in'])->group(function () {
    Route::get('/authors/show/{id}', [AuthorsController::class, 'show']);
    Route::get('/authors/destroy/{id}', [AuthorsController::class, 'destroy']);
    Route::get('/authors/{page?}', [AuthorsController::class, 'index']);

    Route::get('/books/create', [BooksController::class, 'create']);
    Route::post('/books/store', [BooksController::class, 'store'])->name('book.store');
    Route::get('/books/{page?}', [BooksController::class, 'index']);
    Route::get('/books/destroy/{id}', [BooksController::class, 'destroy']);
});