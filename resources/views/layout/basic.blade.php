<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Task</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                margin: 0px;
            }
        </style>

        @stack('custom-head')
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/authors">Authors</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/books">Books  </a>
                </li>
                </ul>
                <div style="text-align: right; margin-bottom:20px">
                    Hello {{ session('token')->user->first_name }} {{ session('token')->user->last_name }} | <a href="\logout">Logout</a>
                    
                </div>
            </div>
        </nav>
        <div style="margin: 20px">
            @yield('content')
        </div>
    </body>
</html>
