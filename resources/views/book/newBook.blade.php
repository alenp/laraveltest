@extends('layout.basic')

    @push('custom-head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    @endpush

@section('content')


    <h1>Create the New Book</h1>
    @if (session('status'))
      <div class="alert alert-danger">
         {!! session('status') !!}
      </div>
    @endif

    <form action="{{ route('book.store') }}" method="POST">
        @csrf
        <div class="form-group row">
            <label for="title" class="col-md-4 col-form-label text-md-right">Title:</label>
            <div class="col-md-6">
                <input type="text" id="title" class="form-control" name="title" required autofocus>
            </div>
        </div>

        <div class="form-group row">
            <label for="author" class="col-md-4 col-form-label text-md-right">Author:</label>
            <div class="col-md-6">
                <select id="author" class="form-control" name="author" required>
                    <option value="">Select Author...</option>
                    @foreach($authors as $author)
                        <option value="{{ $author->id }}">{{ $author->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="release_date" class="col-md-4 col-form-label text-md-right">Release Date:</label>
            <div class="col-md-6">
                <div class="container" style="padding: 0;">
                    <input type="text" id="release_date" class="date form-control" name="release_date" required>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('.date').datepicker({  
                format: 'dd.mm.yyyy.'
            });  
        </script> 

        <div class="form-group row">
            <label for="isbn" class="col-md-4 col-form-label text-md-right">Isbn:</label>
            <div class="col-md-6">
                <input type="text" id="isbn" class="form-control" name="isbn" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="format" class="col-md-4 col-form-label text-md-right">Format:</label>
            <div class="col-md-6">
                <input type="text" id="format" class="form-control" name="format" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="number_of_pages" class="col-md-4 col-form-label text-md-right">Pages:</label>
            <div class="col-md-6">
                <input type="text" id="number_of_pages" class="form-control" name="number_of_pages" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-md-4 col-form-label text-md-right">Description:</label>
            <div class="col-md-6">
                <input type="text" id="description" class="form-control" name="description" required>
            </div>
        </div>

        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Add
            </button>
        </div>
    </form>
@endsection
