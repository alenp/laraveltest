@extends('layout.basic')

@section('content')

    <h1>Books</h1>
    @if (count($errors) > 0)
        @foreach ($errors as $error)
            <div class="alert alert-danger">{{ $error }}</div>
        @endforeach
    @endif
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
    @endif
    <a href="/books/create" style="display:block; padding: 10px; border: 1px;">Add The New Book</a>
    <table class="table table-sm">
    <thead>
        <tr>
            <th>Title</th>
            <th>Release Date</th>
            <th>Isbm</th>
            <th>Format</th>
            <th>Pages</th>
        </tr>
    </thead>
    <tbody>
        @isset($books->items)
            @foreach($books->items as $book)
            <tr>
                <td>{{$book->title}}</td>
                <td>{{\Carbon\Carbon::parse($book->release_date)->format('d.m.Y.')}}</td>
                <td>{{$book->isbn}}</td>
                <td>{{$book->format}}</td>
                <td>{{$book->number_of_pages}}</td>
            </tr>
            @endforeach
        @endisset
    </tbody>

</table>

<!-- pagination -->
@if ($books and $books->total_results and $books->total_results > $books->limit)
    <div style="text-align: center; margin:30px">
        @if ($books->current_page == 1)
            &#60;
        @else
            <a href="/books/{{ $books->current_page-1 }}">&#60;</a>
        @endif
        @for ($i = 1; $i <= $books->total_pages; $i++)
            @if ($i == $books->current_page)
                {{ $i }}
            @else
                <a href="/books/{{ $i }}">{{ $i }}</a>
            @endif
        @endfor
        @if ($books->current_page == $books->total_pages)
            &#62;
        @else
            <a href="/books/{{ $books->current_page+1 }}">&#62;</a>
        @endif

    </div>
@endif


@endsection
