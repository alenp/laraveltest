@extends('layout.basic')

@section('content')

    <h1>Author</h1>
    @if (session('status'))
      <div class="alert alert-danger">
         {!! session('status') !!}
      </div>
    @endif
    @if (count($errors) > 0)
        @foreach ($errors as $error)
            <div class="alert alert-danger">{{ $error }}</div>
        @endforeach
    @else
        <table style="border:0; margin:20px">
        <tbody>
            <tr>
                <th>Name:</th><td>{{ $author->first_name }} {{ $author->last_name }}</td>
            </tr>
            <tr>
                <th>Birthday:</th><td>{{\Carbon\Carbon::parse($author->birthday)->format('d.m.Y.')}}</td>
            </tr>
            <tr>
                <th>Place of Birth:</th><td>{{ $author->place_of_birth }}</td>
            </tr>
            <tr>
                <th>Gender:</th><td>{{ $author->gender }}</td>
            </tr>
            <tr>
                <th>Biography:</th><td>{{ $author->biography }}</td>
            </tr>
        </tbody>
        </table>

        @if (count($author->books) > 0)
        <h2>Books</h2>
            <table class="table table-sm">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Release Date</th>
                    <th>Description</th>
                    <th>Format</th>
                    <th>Pages</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($author->books as $book)
                <tr>
                    <td><a href="/books/show/{{ $book->id }}">{{$book->title}}</a></td>
                    <td>{{\Carbon\Carbon::parse($book->release_date)->format('d.m.Y.')}}</td>
                    <td>{{$book->description}}</td>
                    <td>{{$book->format}}</td>
                    <td>{{$book->number_of_pages}}</td>
                    <td><a href="/books/destroy/{{ $book->id }}">Delete</a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        @else
            <h3>No Books Available</h3>
            <a href="/authors/destroy/{{ $author->id }}" style="padding: 20px;">Delete Author</a>
        @endif

        
    @endif
@endsection
