@extends('layout.basic')

@section('content')

    <h1>Authors</h1>
    @if (count($errors) > 0)
        @foreach ($errors as $error)
            <div class="alert alert-danger">{{ $error }}</div>
        @endforeach
    @endif

    @isset($authors->items)
        <table class="table table-sm">
        <thead>
            <tr>
                <th>Name</th>
                <th>Birthday</th>
                <th>Gender</th>
                <th>Place of Birth</th>
            </tr>
        </thead>
        <tbody>
            @foreach($authors->items as $author)
            <tr>
                <td><a href="/authors/show/{{ $author->id }}">{{$author->first_name}} {{$author->last_name}}</a></td>
                <td>{{\Carbon\Carbon::parse($author->birthday)->format('d.m.Y.')}}</td>
                <td>{{$author->gender}}</td>
                <td>{{$author->place_of_birth}}</td>
            </tr>
            @endforeach
        </tbody>
        </table>
    @endisset

<!-- pagination -->
@if ($authors and $authors->total_results and $authors->total_results > $authors->limit)
    <div style="text-align: center; margin:30px">
        @if ($authors->current_page == 1)
            &#60;
        @else
            <a href="/authors/{{ $authors->current_page-1 }}">&#60;</a>
        @endif
        @for ($i = 1; $i <= $authors->total_pages; $i++)
            @if ($i == $authors->current_page)
                {{ $i }}
            @else
                <a href="/authors/{{ $i }}">{{ $i }}</a>
            @endif
        @endfor
        @if ($authors->current_page == $authors->total_pages)
            &#62;
        @else
            <a href="/authors/{{ $authors->current_page+1 }}">&#62;</a>
        @endif

    </div>
@endif


@endsection
