<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\QSS;

class AddAuthor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:author';

    /** 
     * Sample usage
     * php artisan create:author user password   
     * Alen Cvjetkovic 1978-10-02T05:35:54.365Z "Born long time ago" male Split
     * 
     * */ 

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Authors via API';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $auth = array(
            'email' => $this->ask('Your email?'),
            'password' => $this->ask('Your password?')
        );

        $authorData = array(
            "first_name" => $this->ask('Author first name?'),
            "last_name" => $this->ask('Author last name?'),
            "birthday" => $this->ask('Author birthday?'),
            "biography" => $this->ask('Author biography?'),
            "gender" => $this->choice('Author gender?', ['male', 'female']),
            "place_of_birth" => $this->ask('Author place of birth?')
        );

        $qss = new QSS;
        if ($this->confirm('Do you wish to send data to the API and create the Author?')) {
            if ($qss->storeAuthor($auth, $authorData)){
                return Command::SUCCESS;
            }
        }
        
    }
}
