<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\QSS;

class BooksController extends Controller
{
    protected $qss;

    public function __construct(QSS $qss)
    {
        $this->qss = $qss;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $apge
     * @return \Illuminate\Http\Response
     */
    public function index($page = 1)
    {
        $errors = [];
        $books =[];
        
        try {
            $books = $this->qss->getAllBooks((int)$page);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return redirect('/login');
        } catch (\Throwable $e) {
            $errors[] = 'ERROR FETCHING BOOKS!';
        }

        return view('book/books', ['books' => $books, 'errors' => $errors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $errors = [];
        $allAuthors = [];
        
        try {
            $authors = $this->qss->getAllAuthors();
        } catch (\Throwable $e) {
            $errors[] = 'ERROR FETCHING AUTHORS!';
        }
        
        if ($authors) {
            foreach ($authors->items as $author) {
                $object = new \stdClass();
                $object->id = $author->id;
                $object->name = $author->first_name . ' ' . $author->last_name;
                $allAuthors[] = $object;
            }
        }

        return view('book/newBook', ['authors' => $allAuthors, 'errors' => $errors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->qss->storeBook($request);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return redirect('/login');
        } catch (\Throwable $e) {
            return back()->with(['status' => 'Adding the book failed!']);
        }

        return redirect('/books')->with('message', 'New Book Added!');
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $delete = $this->qss->deleteBook((int)$id);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return redirect('/login');
        } catch (\Throwable $e) {
            return back()->with(['status' => "Error deleting the book!"]);
        }

        return back()->with(['status' => 'Book Deleted!']);
    }
}
