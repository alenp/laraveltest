<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\QSS;

class AuthorsController extends Controller
{
    protected $qss;

    public function __construct(QSS $qss)
    {
        $this->qss = $qss;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $page
     * @return \Illuminate\Http\Response
     */
    public function index($page = 1)
    {
        $errors = [];
        $authors = [];

        try {
            $authors = $this->qss->getAuthors((int)$page);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return redirect('/login');
        } catch (\Throwable $e) {
            $errors[] = 'ERROR FETCHING AUTHORS!';
        }

        return view('author/authors', ['authors' => $authors, 'errors' => $errors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $errors = [];
        $author = [];

        try {
            $author = $this->qss->getAuthor($id);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return redirect('/login');
        } catch (\Throwable $e) {
            $errors[] = 'ERROR FETCHING AUTHORS!';
        }

        return view('author/author', ['author' => $author, 'errors' => $errors]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = (int)$id;

        // check book count, newer trust front end :)
        try {
            $author = $this->qss->getAuthor($id);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return redirect('/login');
        } catch (\Throwable $e) {
            $errors[] = 'ERROR FETCHING AUTHORS!';
            return false;
        }

        if (count($author->books)>0) {
            $errors[] = 'CAN NOT DELETE, AUTHOR HAS BOOKS!';
            return view('author/author', ['author' => $author, 'errors' => $errors]);
        }

        try {
            $delete = $this->qss->deleteAuthor($id);
        } catch (\Illuminate\Auth\AuthenticationException $e) {
            return redirect('/login');
        } catch (\Throwable $e) {
            $errors[] = 'ERROR DELETING THE AUTHOR!';
        }

        return redirect('/authors');
    }
}
