<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\QSS;
use App\Exception;

class AuthController extends Controller
{

    protected $qss;

    public function __construct(QSS $qss)
    {
        $this->qss = $qss;
    }

    /**
     * Login 
     *
     * @return response()
     */
    public function loginForm(Request $request) {
        if (session('token')){
            return Redirect('/authors');
        }
  
        return view('login');
    }

    /**
     * POST to login
     *
     * @return response()
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        if ($this->qss->login($request)) {
            return redirect('/authors');
        }
  
        return redirect("/login");
    }

    /**
     * Log out link
     *
     * @return response()
     */
    public function logout(Request $request) {
        $request->session()->flush();
  
        return Redirect('/login');
    }
}
