<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureTokenIsValid
{
    /**
     * Allowing routes only if token exists
     * Token expriation date is checked in QSS Service only if API rejects connection
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!session('token')) {
            return redirect('login');
        }

        return $next($request);
    }
}
