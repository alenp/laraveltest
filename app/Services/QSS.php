<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class QSS
{
    /**
     * Log in / get token from to QSS
     *
     * @return boolean
     */
    public function login(Request $request)
    {
        $response = Http::withoutVerifying()->post(config('services.qss.server') . '/api/v2/token', [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);

        if ($response->getStatusCode() == 200){
            $content = json_decode($response->body());
            session(['token' => $content]);
            return true;
        }
        return false;
    }

    /**
     * Get authors list from QSS
     * @param  int  $page
     * @return mixed
     */
    public function getAuthors($page)
    {
        $response = Http::withoutVerifying()->withToken(session('token')?->token_key)
            ->get(config('services.qss.server') . '/api/v2/authors?&orderBy=firstName&page=' . $page);

        if ($response->getStatusCode() == 200){
            return json_decode($response->body());
        }
        $this->checkTokenExpired();
        return false;
    }

    /**
     * Get authors list from QSS for drop down
     * @return mixed
     */
    public function getAllAuthors()
    {
        $response = Http::withoutVerifying()->withToken(session('token')?->token_key)
            ->get(config('services.qss.server') . '/api/v2/authors?limit=5000&orderBy=firstName');

        if ($response->getStatusCode() == 200){
            return json_decode($response->body());
        }
        $this->checkTokenExpired();
        return false;
    }

    /**
     * Get single author with books
     *
     * @return mixed
     */
    public function getAuthor($id)
    {
        $response = Http::withoutVerifying()->withToken(session('token')?->token_key)
            ->get(config('services.qss.server') . '/api/v2/authors/' . $id);

        if ($response->getStatusCode() == 200){
            return json_decode($response->body());
        }
        $this->checkTokenExpired();
        return false;
    }

    /**
     * List all books from QSS (not required but handy)
     * @param  int  $page
     * @return mixed
     */
    public function getAllBooks($page)
    {
        $response = Http::withoutVerifying()
            ->withToken(session('token')?->token_key)
            ->get(config('services.qss.server') . '/api/v2/books?orderBy=id&direction=DESC&page=' . $page);

        if ($response->getStatusCode() == 200){
            return json_decode($response->body());
        }
        $this->checkTokenExpired();
        return false;
    }

    /**
     * Create new book in QSS
     *
     * @return boolean
     */
    public function storeBook(Request $request)
    {

        $author = new \stdClass();
        $author->id = (int)$request->input('author');

        $dateObj = new \DateTimeImmutable($request->input('release_date'));
        $releseDate = $dateObj->format('Y-m-d\TH:i:sp');

        $response = Http::withoutVerifying()->withToken(session('token')?->token_key)->post(config('services.qss.server') . '/api/v2/books', [
            'title' => $request->input('title'),
            'author' => $author,
            'release_date' => $releseDate,
            'description' => $request->input('description'),
            'isbn' => $request->input('isbn'),
            'format' => $request->input('format'),
            'number_of_pages' => (int)$request->input('number_of_pages'),
        ]);

        if ($response->getStatusCode() == 200){
            return true;
        }

        throw new \ErrorException('API rejected storing the book!');
    }

    /**
     * Create new author in QSS via artisan command
     * Usage: php artisan create:author first_name last_name birthday biography gender place_of_birth
     * 
     * @param  array  $auth
     * @param  array  $authorData
     * @return boolean
     */
    public function storeAuthor(array $auth, array $authorData)
    {
        $content = null;
        $response = Http::withoutVerifying()->post(config('services.qss.server') . '/api/v2/token', [
            'email' => $auth['email'],
            'password' => $auth['password']
        ]);

        if ($response->getStatusCode() != 200){
            echo "Auth failed!\n";
            return false;    
        }
        $content = json_decode($response->body());
        
        // Fix date format
        $dateObj = new \DateTimeImmutable($authorData['birthday']);
        $authorData['birthday'] = $dateObj->format('Y-m-d\TH:i:sp');

        $response = Http::withoutVerifying()->withToken($content->token_key)->post(config('services.qss.server') . '/api/v2/authors', $authorData);

        if ($response->getStatusCode() == 200){
            echo "The author is stored!\n";
            return true;
        }

        echo "Error storing the author\n";
        return false;
    }
  
    /**
     * Delete author
     * @param  int  $id
     * 
     * @return boolean
     */
    public function deleteAuthor(int $id)
    {
        $response = Http::withoutVerifying()->withToken(session('token')?->token_key)->delete(config('services.qss.server') . '/api/v2/authors/' . $id);

        if ($response->getStatusCode() == 204){
            return true;
        }

        throw new \ErrorException('Could not delete the Author!');
    }

    /**
     * Delete book
     * @param  int  $id
     * 
     * @return boolean
     */
    public function deleteBook(int $id)
    {
        $response = Http::withoutVerifying()->withToken(session('token')?->token_key)->delete(config('services.qss.server') . '/api/v2/books/' . $id);

        if ($response->getStatusCode() == 204){
            return true;
        }

        throw new \ErrorException('Could not delete the Book!');
    }


    /**
     * Check if token has expired only if connection is rejected
     * @return bool
     */
    private function checkTokenExpired()
    {
        if (!session('token')){
            return false;
        }
        
        $expires = \Carbon\Carbon::parse(session('token')->expires_at);
        $now =  \Carbon\Carbon::now();

        if ($expires->lt($now)) {
            session()->flush();
            throw new  \Illuminate\Auth\AuthenticationException('Session expired');
        }
    }
}
